import requests
import logging
from bs4 import BeautifulSoup

class CrawlerRequests(object):


    def get_products_page(self, link):
        try:
            response = requests.get(link, verify=False, headers={'User-Agent': 'Mozilla/5.0'})
            response.encoding = 'utf-8'
            html = response.text
            print('Pagina acessada com sucesso!')

            return self.__get_href_products(html)
        except Exception as exc:
            print('Erro request_save : {}'.format(str(exc)))

    def __get_href_products(self, html):
        try:
            soup = BeautifulSoup(html, 'html.parser')

            products_list = soup.select_one("div[class='lista-produto prateleira']")
            elements_href_list = products_list.select("a[class='link url']")
            href_list = []

            for href in elements_href_list:
                href_list.append(href['href'])

            return href_list
        except Exception as exc:
            print("Erro ao acessar href produtos: {}".format(str(exc)))
            return []