from pymongo import MongoClient
import datetime


class Mongodb(object):

    def __init__(self):
        client = MongoClient('mongodb://localhost:27017')
        self.database = client.scraped

    def insert_product(self, info):
        try:
            info['date'] = datetime.datetime.now()
            product = self.database.product
            id = product.insert_one(info).inserted_id
            print('Info adicionada: {}'.format(str(id)))
            print('\n')
        except Exception as exc:
            print('Erro ao inserir: {}'.format(str(exc)))


    def get_all(self):
        try:
            products = []
            product = self.database.product
            cursor = product.find({})
            for document in cursor:
                products.append(document)

            return products
        except Exception as exc:
            print('Erro ao buscar todos produtos: {}'.format(str(exc)))