﻿# SECOMP-Petz

Projeto para fins didáticos de web crawling para Semana da Computação UFSCAR

## Docker

### RabbitMQ
docker run --rm -p 5672:5672 -p 8080:15672 rabbitmq:3-management

### MongoDB
docker run -d -p 27017:27017 -p 28017:28017 -e AUTH=no tutum/mongodb