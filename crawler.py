import codecs
import requests
from connections.rabbit import Rabbit
from crawler_selenium import CrawlerSelenium
from crawler_requests import CrawlerRequests
import uuid
import urllib3
import time


class Crawler(CrawlerRequests):

    def __init__(self):
        super().__init__()
        self.rabbit = Rabbit()

    def start(self):
        try:
            urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
            all_href_products = self.paginate(max_page=5)
            self.__inter_links(all_href_products)

        except Exception as exc:
            print('Erro ao acessar links: {}'.format(str(exc)))

    def paginate(self, max_page=999999):
        try:
            all_href_products = []
            main_link = 'https://www.extra.com.br/PetShop/cachorro/?Filtro=C2294_C2295&paginaAtual={}'
            page = 1

            while page <= max_page:
                href_products = self.get_products_page(main_link.format(str(page)))
                if not href_products:
                    break

                all_href_products += href_products
                page += 1

            return all_href_products
        except Exception as exc:
            print('Erro ao acessar link direto: {}'.format(str(exc)))

    def __inter_links(self, href_links):
        try:
            for href in href_links:
                self.__request_save_link(href)

        except Exception as exc:
            print('Erro do loop href: {}'.format(str(exc)))

    def __request_save_link(self, href):
        try:
            response = requests.get(href, verify=False, headers={'User-Agent': 'Mozilla/5.0'})
            response.encoding = 'utf-8'
            html = response.text
            self.__save_html_file(html)
        except Exception as exc:
            print('Erro request_save : {}'.format(str(exc)))

    def __save_html_file(self, html):
        try:
            name = str(uuid.uuid4())
            path = './files/{}.html'.format(name)

            with codecs.open(path, 'w', encoding='utf8') as file:
                file.write(html)
                print('Salvo arquivo {}'.format(name))

            self.rabbit.send_message(path)

        except Exception as exc:
            print('Erro ao salvar HTML: {}'.format(str(exc)))


if __name__ == "__main__":
    start = time.time()
    Crawler().start()
    end = time.time()
    print('Tempo em execução: {}'.format(str(end - start)))
